"use strict";
// Load plugins
const autoprefixer = require("gulp-autoprefixer");
const browsersync = require("browser-sync").create();
const path = require("path");
const cleanCSS = require("gulp-clean-css");
const del = require("del");
const gulp = require("gulp");
const header = require("gulp-header");
const merge = require("merge-stream");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const pug = require("gulp-pug");
const data = require("gulp-data");
const uglify = require("gulp-uglify");
// var replace = require('gulp-replace');

// Load package.json for banner
const pkg = require("./package.json");

// Set the banner content
const banner = [
  "/*!\n",
  " * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n",
  " * Copyright 2013-" + new Date().getFullYear(),
  " <%= pkg.author %>\n",
  " * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n",
  " */\n",
  "\n",
].join("");
const paths = {
  public: "./public/",
  sass: "./src/scss/",
  css: "./public/css/",
  data: "./src/_data/",
  js: "./src/js/",
  jt_dest: "./public/js/",
};
// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./public",
    },
    port: 3000,
  });
  done();
}

// BrowserSync reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean vendor
function clean() {
  return del(["./public/vendor/"]);
}

// Bring third party dependencies from node_modules into vendor directory
function modules() {
  // Bootstrap JS
  var bootstrapJS = gulp
    .src("./node_modules/bootstrap/dist/js/*")
    .pipe(gulp.dest("./public/vendor/bootstrap/js"));
  // Bootstrap SCSS
  var bootstrapSCSS = gulp
    .src("./node_modules/bootstrap/scss/**/*")
    .pipe(gulp.dest("./public/vendor/bootstrap/scss"));

  // Font Awesome
  var fontAwesome = gulp
    .src("./node_modules/@fortawesome/**/*")
    .pipe(gulp.dest("./public/vendor"));

  // Slick Carousel
  var slick = gulp
    .src("./node_modules/slick-carousel/slick/**/*")
    .pipe(gulp.dest("./public/vendor/slick"));
  // jQuery
  var jquery = gulp
    .src([
      "./node_modules/jquery/dist/*",
      "!./node_modules/jquery/dist/core.js",
    ])
    .pipe(gulp.dest("./public/vendor/jquery"));
  return merge(bootstrapJS, bootstrapSCSS, fontAwesome, jquery, slick);
}

// CSS task
function css() {
  return gulp
    .src("./src/scss/**/*.scss")
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "expanded",
        includePaths: ["./node_modules"],
      })
    )
    .on("error", sass.logError)
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(gulp.dest(paths.css))
    .pipe(gulp.dest(paths.css))
    .pipe(browsersync.stream());
}

// HTML task
function html() {
  const g = gulp
    .src("./src/*.pug")
    .pipe(
      data(function (file) {
        return require(paths.data + path.basename(file.path) + ".json");
      })
    )
    .pipe(
      pug({
        pretty: true,
      })
    )
    .on("error", function (err) {
      process.stderr.write(err.message + "\n");
      this.emit("end");
    })
    .pipe(gulp.dest(paths.public));

  if (process.env.ENV == "prod") {
    const $path_site_img = "https://www.grab.com/sg/wp-content/uploads/";
    g.pipe(
      replace(
        /img\/([a-zA-Z0-9\@.-]+)\/([a-zA-Z0-9\@.-]+)/g,
        $path_site_img + "$2"
      )
    ).pipe(replace(/img\/([a-zA-Z0-9-]+)/g, $path_site_img + "$1"));
  }
  return g;
}

// Pug reload
const rebuilt = gulp.series(html, browserSyncReload);

// JS task
function js() {
  return gulp
    .src([paths.js + "**/*.js", paths.js + "*.js"])
    .pipe(uglify())
    .pipe(
      header(banner, {
        pkg: pkg,
      })
    )
    .pipe(
      rename({
        // suffix: '.min'
      })
    )
    .pipe(gulp.dest(paths.jt_dest))
    .pipe(browsersync.stream());
}

// Watch files
function watchFiles() {
  gulp.watch("./src/scss/**/*", css);
  gulp.watch(paths.js + "**/*", js);
  gulp.watch("./src/**/*.pug", rebuilt);
}

// Define complex tasks
const vendor = gulp.series(clean, modules);
const build = gulp.series(vendor, gulp.parallel(css, js, html));
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// Export tasks
exports.css = css;
exports.html = html;
exports.js = js;
exports.clean = clean;
exports.vendor = vendor;
exports.build = build;
exports.watch = watch;
exports.default = build;
